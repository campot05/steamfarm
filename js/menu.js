(() => {
  const menuBtnRef = document.querySelector('[data-menu-button]');
  const mobileMenuRef = document.querySelector('[data-menu]');
  const mobileBodyRef = document.querySelector('[data-body]');

  const toggleMenu = () => {
    if (window.matchMedia('(max-width: 920px)').matches) {
      menuBtnRef.classList.toggle('hamburger--is-active');
      mobileBodyRef.classList.toggle('scroll-off');
      mobileMenuRef.classList.toggle('open');
    }
  };

  menuBtnRef.addEventListener('click', toggleMenu);

  mobileMenuRef.addEventListener('click', (event) => {
    if (
      event.target.closest('.header__nav-link') &&
      window.matchMedia('(max-width: 920px)').matches
    ) {
      toggleMenu();
    }
  });

  document.addEventListener('click', (event) => {
    if (
      !event.target.closest('[data-menu-button]') &&
      !event.target.closest('[data-menu]') &&
      window.matchMedia('(max-width: 920px)').matches
    ) {
      if (mobileMenuRef.classList.contains('open')) {
        toggleMenu();
      }
    }
  });
})();
