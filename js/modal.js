document.addEventListener('DOMContentLoaded', () => {
  const select = document.querySelector('.custom-select');
  if (select) {
    const selected = select.querySelector('.selected');
    const optionsList = select.querySelectorAll('.custom-option');
    const toggleSelect = () => select.classList.toggle('open');
    const chooseOption = (option) => {
      selected.textContent = option.textContent;
      select.classList.remove('open');
    };

    selected.addEventListener('click', toggleSelect);
    optionsList.forEach((option) => {
      option.addEventListener('click', () => chooseOption(option));
    });

    document.addEventListener('click', (e) => {
      if (!select.contains(e.target)) {
        select.classList.remove('open');
      }
    });
  }

  function toggleModal(modalSelector, isOpen) {
    const modal = document.querySelector(modalSelector);
    if (!modal) return;
    document.body.classList.toggle('modal-open', isOpen);
    modal.classList.toggle('is-hidden', !isOpen);
  }

  const openModalBtns = document.querySelectorAll('[data-modal-open]');
  const closeModalBtn = document.querySelector('[data-modal-close]');
  const backdrop = document.querySelector('.backdrop[data-modal]');

  openModalBtns.forEach((btn) =>
    btn.addEventListener('click', () => toggleModal('[data-modal]', true))
  );
  if (closeModalBtn)
    closeModalBtn.addEventListener('click', () =>
      toggleModal('[data-modal]', false)
    );
  if (backdrop)
    backdrop.addEventListener('click', (event) => {
      if (event.target === backdrop) toggleModal('[data-modal]', false);
    });

  const openShoppingModalBtns = document.querySelectorAll(
    '[data-modal-shopping-open]'
  );
  const closeShoppingModalBtn = document.querySelector(
    '[data-modal-shopping] [data-modal-close]'
  );
  const shoppingBackdrop = document.querySelector(
    '.backdrop[data-modal-shopping]'
  );

  openShoppingModalBtns.forEach((btn) =>
    btn.addEventListener('click', () =>
      toggleModal('[data-modal-shopping]', true)
    )
  );
  if (closeShoppingModalBtn)
    closeShoppingModalBtn.addEventListener('click', () =>
      toggleModal('[data-modal-shopping]', false)
    );
  if (shoppingBackdrop)
    shoppingBackdrop.addEventListener('click', (event) => {
      if (event.target.matches('.backdrop[data-modal-shopping]'))
        toggleModal('[data-modal-shopping]', false);
    });
});

document.addEventListener('DOMContentLoaded', () => {
  const footerLinks = document.querySelectorAll('.footer__nav .nav-link');

  footerLinks.forEach((link) => {
    link.addEventListener('click', (e) => {
      if (link.getAttribute('href') === '#') {
        e.preventDefault();
      }
    });
  });
});
